#!/usr/bin/env python

def solution2(array):
    '''
    Given a list of unsorted positive integers, return a list of indicices
    which represent the start of a run.

    A run is defined to be consecutive ascending or descending numbers. I assumed that
    the terms are considered strict, i.e. strictly ascending such that 9,10,10,11 is
    not a single run, but 9,10 and 10, 11 in the above set are considered 2 runs.
    '''
    if not isinstance(array, list):
        raise TypeError('Input is %s. Please input a list'%type(array))

    indicies = []
    for i,x in enumerate(array):
        if i == 0:
            last_diff = 0
            continue
        diff = x - array[i-1]
        if abs(diff) == 1:
            if diff != last_diff:
                indicies.append(i-1)
        last_diff = diff
    return indicies
