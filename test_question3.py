#!/usr/bin/env python
import json
import urllib2
import cookielib

from bs4 import BeautifulSoup as BS

def scrape(st_number=None, st_name=None, st_suffix=None,
        unit=None, block=None, lot=None):
    '''
    Question 3

    Build and document a php or python-based script which extracts Electrical
    Permit information from the San Francisco Department of Buildings
    (http://dbiweb.sfgov.org/dbipts/Default2.aspx?page=addressquery).

    Use the following address as your search parameters:
    Address: 555 CALIFORNIA ST      Block/Lot: 0259 / 026
    '''
    st_number = st_number or ''
    st_name = st_name or ''
    st_suffix = st_suffix or ''
    unit = unit or ''
    block = block or '0259'
    lot = lot or '026'
    uri = "http://dbiweb.sfgov.org/dbipts/?page=address&StreetNumber={0}&"\
        "StreetName={1}&StreetSuffix={2}&Unit={3}&Block={4}&Lot={5}".format(
                    st_number, st_name, st_suffix, unit, block, lot)

    ##########################################################################
    # We must set up the request with proper cookies in order to properly send
    #  our requested information (block and lot info) through the from. 
    ##########################################################################
    cookies = cookielib.LWPCookieJar()

    opener = urllib2.build_opener(urllib2.HTTPHandler(),
            urllib2.HTTPSHandler(), urllib2.HTTPCookieProcessor(cookies))

    req = urllib2.Request(uri)
    res = opener.open(req)

    cookie = None
    for cookie in cookies:
        cookie = 'ASP.NET_SessionId=' + cookie.value
        break

    #Open permits page for query address
    uri = 'http://dbiweb.sfgov.org/dbipts/default.aspx?page=' + \
            'AddressData2&ShowPanel=EID'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) ' + \
                'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/' +\
                '42.0.2311.90 Safari/537.36',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,' + \
                'image/webp,*/*;q=0.8',
        'Referer': 'http://dbiweb.sfgov.org/dbipts/Default2.aspx?page=' + \
                'addressquery',
        'Cookie': cookie,
        'Connection': 'keep-alive'}
    req = urllib2.Request(uri, "", headers)
    response = urllib2.urlopen(req)
    html = BS(response.read())

    ##########################################################################
    # The response page requires an event in order to see all permit
    #   information.
    ##########################################################################

    # Special data that must be sent via a POST request to get all information
    VIEWSTATE = html.findAll('input', {'id': '__VIEWSTATE'})[0]["value"]
    VIEWSTATEGENERATOR = html.findAll(
            'input', {'id': '__VIEWSTATEGENERATOR'})[0]["value"]
    EVENTVALIDATION = html.findAll(
            'input', {'id': '__EVENTVALIDATION'})[0]["value"]
    uri = "http://dbiweb.sfgov.org/dbipts/Default2.aspx?page=AddressData2&" + \
            "ShowPanel=EID"
    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;' + \
            'q=0.9,image/webp,*/*;q=0.8',
               'Accept-Encoding': 'gzip, deflate',
               'Accept-Language': 'en-US,en;q=0.8,fr;q=0.6,pt;q=0.4,es;q=0.2',
               'Cache-Control': 'max-age=0', 'Connection': 'keep-alive',
               'Content-Type': 'application/x-www-form-urlencoded',
               'Cookie': cookie,
               'Origin': 'http://dbiweb.sfgov.org',
               'Referer': 'http://dbiweb.sfgov.org/dbipts/default.aspx?' + \
                       'page=AddressData2&ShowPanel=EID',
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X ' + \
                       '10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) ' + \
                        'Chrome/42.0.2311.90 Safari/537.36'}
    data = "__EVENTTARGET=InfoReq1%24btnEidShowAll&__EVENTARGUMENT=" + \
            "&__VIEWSTATE=" + VIEWSTATE + "&__VIEWSTATEGENERATOR=" + \
            VIEWSTATEGENERATOR + "&__EVENTVALIDATION=" + EVENTVALIDATION
    data = data.replace("+", "%2B")
    req = urllib2.Request(uri, data, headers)
    response = urllib2.urlopen(req)

    ##########################################################################
    #At this point, the final response is ready to be read and the table to be
    #   parsed
    ##########################################################################
    html = BS(response.read())
    table = html.findAll('table', {'id': 'InfoReq1_dgEID'})
    if not len(table): return {} 
    soup_table = BS(str(table[0]))

    # The column names have to be indexed as follows:
    td_column_names = ['Permit #', 'Block', 'Lot', 'Street #', 'Street Name', 
            'Unit', 'Current Stage', 'Stage Data']
    # Each permit will be a dictionary with keys from td_column_names
    permits = []
    for row in soup_table.find_all("tr"):
        permit = {}
        for td_col_index, table_data in enumerate(BS(str(row)).find_all("td")):
            table_data_text = table_data.get_text().strip()
            # Strip away unicode nonesense
            s = ''.join([i if ord(i) < 128 else ' ' for i in table_data_text])
            permit[td_column_names[td_col_index]] = s 
        if permit:
            permits.append(permit)

    return permits

if __name__ == '__main__':
    permits = scrape()
    permit_json = json.dumps(permits)
    json_file_nm = 'permits.json'
    with open(json_file_nm, 'w') as f:
        f.write(permit_json)
    if permits:
        print('Wrote %d permits as a json to %s'%(len(permits), json_file_nm))
