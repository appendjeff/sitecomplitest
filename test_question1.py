#!/usr/bin/env python

import unittest

from question1 import get_desc

class TestQuestion1(unittest.TestCase):

    not_found = 'Not found'
    descriptions = {'R': 'General Residence Districts',
        'C': 'Commercial Districts',
        'M_1': 'Manufacturing Districts',
        'M_2': 'Mixed Manufacturing & Residential Districts',
        'BPC': 'Battery Park City',
        'PARK': 'New York City Parks',
        'PARKNYS': 'New York State Parks',
        'PARKUS': 'United States Parks',
        'ZNA': 'Zoning Not Applicable',
        'ZR 11-151': 'Special Zoning District'}

    def test_empty(self):
        code = ''
        self.assertEqual(get_desc(code), self.not_found)

    def test_r1(self):
        code = 'R1-1'
        self.assertEqual(get_desc(code), self.descriptions['R'])

    def test_r2(self):
        code = 'R1a-1'
        self.assertEqual(get_desc(code), self.not_found)

    def test_r3(self):
        code = 'R10-10'
        self.assertEqual(get_desc(code), self.descriptions['R'])

    def test_r4(self):
        code = 'R3C'
        self.assertEqual(get_desc(code), self.descriptions['R'])

    def test_c1(self):
        code = 'C1-6'
        self.assertEqual(get_desc(code), self.descriptions['C'])

    def test_c2(self):
        code = 'C2-7'
        self.assertEqual(get_desc(code), self.not_found)

    def test_c3(self):
        code = 'C8-5'
        self.assertEqual(get_desc(code), self.not_found)

    def test_m1_1(self):
        code = 'M1-s5'
        self.assertEqual(get_desc(code), self.not_found)

    def test_m1_2(self):
        code = 'M2-2'
        self.assertEqual(get_desc(code), self.descriptions['M_1'])

    def test_m2_1(self):
        code = 'M1-4/R7'
        self.assertEqual(get_desc(code), self.descriptions['M_2'])

    def test_m2_2(self):
        code = 'M1-6/R10'
        self.assertEqual(get_desc(code), self.descriptions['M_2'])

    def test_m2_3(self):
        code = 'M3/R10'
        self.assertEqual(get_desc(code), self.not_found)

    def test_park(self):
        code = 'PARKNYS'
        self.assertEqual(get_desc(code), self.descriptions[code])

    def test_special(self):
        code = 'ZR 11-151'
        self.assertEqual(get_desc(code), self.descriptions[code])


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestQuestion1)
    unittest.TextTestRunner(verbosity=2).run(suite)
