#!/usr/bin/env python

import re
import json
from string import ascii_uppercase as a_up

descriptions = {'R': 'General Residence Districts',
        'C': 'Commercial Districts',
        'M_1': 'Manufacturing Districts',
        'M_2': 'Mixed Manufacturing & Residential Districts',
        'BPC': 'Battery Park City',
        'PARK': 'New York City Parks',
        'PARKNYS': 'New York State Parks',
        'PARKUS': 'United States Parks',
        'ZNA': 'Zoning Not Applicable',
        'ZR 11-151': 'Special Zoning District'}

def upper_in(char, lower, upper):
    '''
    Given a char determine if it is inside the ascii_uppercase bound
    '''
    return a_up.find(lower) <= a_up.find(char) <= a_up.find(upper)

def bad_int_conversion(x,y):
    try:
        if str(int(x)) != x or str(int(y)) != y:
            return True
    except ValueError:
        return True
    
def get_desc(code):
    '''
    Returns the description of the code, if it exists in the
    descriptions dictionary. If it does not exist, return
    "Not Found"
    '''

    try:
        return descriptions[code]
    except KeyError:
        pass

    not_found = 'Not found'
    if not code:
        return not_found

    if code[0] == 'R':
        #R1-1 - R10H
        des = descriptions['R']
        first_option = code[1:].split('-')
        if len(first_option) == 2:
            # code is of form R1-1 or R10-10
            x,y = first_option
            if bad_int_conversion(x,y): return not_found
            if 1 <= int(x) <= 10 and 1 <= int(y) <= 10:
                return des
        other_option = re.findall(r'(\d+)(\w)', code[1:])
        if other_option:
            # code is of type R10H
            x, y = other_option[0]
            if 1 <= int(x) <= 10 and upper_in(y, 'A', 'H'):
                return des
            
    elif code[0] == 'C':
        #C1-6 - C8-4
        # Assume that C1-7 not in range, but C2-6 is
        des = descriptions['C']
        option = code[1:].split('-')
        if len(option) != 2: return not_found
        x,y = option
        if bad_int_conversion(x,y): return not_found
        x,y = int(x), int(y)
        if (x,y) == (1,6):
            return des
        elif 1 <= x <= 7 and 1 <= y <= 6:
            return des
        elif x == 8 and 1 <= y <= 4:
            return des

    elif code[0] == 'M':
        if code.find('/') == -1:
            #M1-1/R5 - M1-6/R10
            des = descriptions['M_1']
            option = code[1:].split('-')
            if len(option) != 2: return not_found
            x,y = code[1:].split('-')
            if bad_int_conversion(x,y): return not_found
            x,y = int(x), int(y)
            if 1 <= x <= 3 and 1 <= y <= 2:
                return des

        else:
            #M1-1/R5 - M1-6/R10
            des = descriptions['M_2']
            option = code[1:].split('/')
            if len(option) != 2: return not_found
            first, second = option
            # first is of type 1-1 and second is of type R5
            splitted = first.split('-')
            if len(splitted) != 2:
                return not_found
            x,y = splitted
            if bad_int_conversion(x,y): return not_found
            x,y = int(x), int(y)
            first_truth = x == 1 and 1 <= y <= 6
            try:
                second_num = int(second[1:])
            except ValueError:
                return not_found
            second_truth = second[0] == 'R' and \
                            second[1:] == str(second_num) and \
                            5 <= second_num <= 10
            if first_truth and second_truth:
                return des

    return not_found


def main(codes):
    '''Write the json to disk and print it to console'''
    results = [dict(code=code, description=get_desc(code)) for code in codes]
    json_str = json.dumps({'codes': results})
    with open('question1_output.json', 'w') as f:
        f.write(json_str)
    print json_str

if __name__ == '__main__':
    given_codes = ['M3', 'R3-2', 'PARKNYS', 'M1-3/R9']
    harder_test = ['R7A','R8A','C4-4A','M3-2','R8B','C1-6A','R7B','R8X',
            'C1-7A','PARK','C1-9A','R6','C1-7','C2-6','R10','C4-5','C6-3X',
            'C1-6','C6-2M','C6-4M','M2-4','M1-5/R7X']
    main(harder_test)
