#!/usr/bin/env python

import unittest

from question2 import solution2

class TestQuestion2(unittest.TestCase):

    def test_empty(self):
        array = []
        self.assertEqual(solution2(array), [])

    def test_duplicates(self):
        array = [1,1,1,1,1]
        self.assertEqual(solution2(array), [])

    def test_descending_many(self):
        array = [5, 3, -50, -51]
        self.assertEqual(solution2(array), [2])

    def test_ascending_many(self):
        array = [-4, 3, 50, 51]
        self.assertEqual(solution2(array), [2])

    def test_middle_hump(self):
        array = [1, 2, 1, 1, 0]
        self.assertEqual(solution2(array), [0, 1, 3])

    def test_given(self):
        array = [1, 1, 3, 5, 6, 8, 10, 11, 10, 9, 8, 9, 10, 11, 7]
        self.assertEqual(solution2(array), [3,6,7,10])

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestQuestion2)
    unittest.TextTestRunner(verbosity=2).run(suite)
